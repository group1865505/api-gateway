const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');

const app = express();

const cors = require('cors');
const corsOptions = {
  origin: '*' ,
  credentials: true,
  'allowedHeaders': ['sessionId', 'Content-type'],
  'exposedHeaders': ['sessionId'],
  'methods': 'GET, HEAD, PUT, PATCH, POST, DELETE',
  'preflightContinue': false
}
app.use(cors(corsOptions));

app.use('/api/produits', createProxyMiddleware({ target: 'http://host.docker.internal:5002', changeOrigin: true }));
app.use('/api/commandes', createProxyMiddleware({ target: 'http://host.docker.internal:5000', changeOrigin: true }));
app.use('/api/paiement', createProxyMiddleware({ target: 'http://host.docker.internal:5001', changeOrigin: true }));

const port = 7000;
app.listen(port, () => {
  console.log(`Proxy server is running on port ${port}`);
});

