FROM node:16.15.0-alpine
ENV NODE_ENV=production
WORKDIR /app

COPY ["package.json", "package-lock.json", "./"]
RUN npm install --${NODE_ENV}

COPY . .

EXPOSE 7000

CMD ["node", "gateway.js"]